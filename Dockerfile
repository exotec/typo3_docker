FROM php:7.4-apache-buster
LABEL maintainer="Alexander Weber <weber@exotec.de>"

ENV TYPO3_VERSION=9

WORKDIR .

# Install TYPO3
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    wget \
    ghostscript \
# Configure PHP
    libxml2-dev libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libpq-dev \
    libzip-dev \
    zlib1g-dev \
# Install required 3rd party tools
    graphicsmagick && \
# Configure extensions
    docker-php-ext-configure gd --with-libdir=/usr/include/ --with-jpeg --with-freetype && \
    docker-php-ext-install -j$(nproc) mysqli soap gd zip opcache intl pgsql pdo_pgsql && \
    pecl install xdebug && \
    echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo 'memory_limit=256M\nalways_populate_raw_post_data = -1\nmax_execution_time = 240\nmax_input_vars = 1500\nupload_max_filesize = 32M\npost_max_size = 32M'  >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo 'zend_extension="/usr/local/lib/php/extensions/no-debug-non-zts-20190902/xdebug.so"' >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo "xdebug.remote_autostart=1" >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo "xdebug.idekey=docker" >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo "xdebug.max_nesting_level = 400" >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo "xdebug.remote_host=$IP_ADDRESS" >> /usr/local/etc/php/conf.d/typo3.ini \
    && echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/typo3.ini \
# Configure Apache as needed
    a2enmod rewrite && \
    apt-get clean && \
    apt-get -y purge \
    libxml2-dev libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libzip-dev \
    zlib1g-dev && \
    rm -rf /var/lib/apt/lists/* /usr/src/*

# Configure volumes
VOLUME /var/www/html